/*
 * Copyright 2016 Cagdas Caglak
 * Updated 2019 JFrey
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.Sensors;

import android.hardware.Sensor;

import com.google.android.things.userdriver.UserDriverManager;
import com.google.android.things.userdriver.sensor.UserSensor;
import com.google.android.things.userdriver.sensor.UserSensorReading;

import java.io.IOException;
import java.util.UUID;

public class HMC5883LSensorDriver implements AutoCloseable {

    private static final String TAG = HMC5883LSensorDriver.class.getSimpleName();

    private static final String DRIVER_VENDOR = "";
    private static final String DRIVER_NAME = "HMCL5883L";
    private static final int DRIVER_VERSION = 1;
    private static final int DRIVER_RESOLUTION = 8; //±8 Gauss
    private static final float DRIVER_POWER = 3.6f; // Volt
    private static final int DRIVER_MAX_DELAY_US = Math.round(1000000.f / 0.75f);
    private static final int DRIVER_MIN_DELAY_US = Math.round(1000000.f / 75f);

    private HMC5883L hmc5883L;
    private MagnetometerUserDriver mUserDriver;

    public HMC5883LSensorDriver(String bus) throws IOException {
        hmc5883L = new HMC5883L(bus);
    }

    public void registerMagnetometerSensor() {
        if (hmc5883L == null) {
            throw new IllegalStateException("cannot register closed driver");
        }

        if (mUserDriver == null) {
            mUserDriver = new MagnetometerUserDriver();
            UserDriverManager.getInstance().registerSensor(mUserDriver.getUserSensor());
        }
    }

    public void unregisterMagnetometerSensor() {
        if (mUserDriver != null) {
            UserDriverManager.getInstance().unregisterSensor(mUserDriver.getUserSensor());
            mUserDriver = null;
        }
    }

    @Override
    public void close() throws Exception {
        unregisterMagnetometerSensor();
        if (hmc5883L != null) {
            try {
                hmc5883L.close();
            } finally {
                hmc5883L = null;
            }
        }
    }

    private class MagnetometerUserDriver {

        private UserSensor mUserSensor;

        private UserSensor getUserSensor() {
            if (mUserSensor == null) {
                mUserSensor = new UserSensor.Builder()
                        .setType(Sensor.TYPE_MAGNETIC_FIELD)
                        .setName(DRIVER_NAME)
                        .setVendor(DRIVER_VENDOR)
                        .setVersion(DRIVER_VERSION)
                        .setResolution(DRIVER_RESOLUTION)
                        .setPower(DRIVER_POWER)
                        .setMinDelay(DRIVER_MIN_DELAY_US)
                        .setMaxDelay(DRIVER_MAX_DELAY_US)
                        .setUuid(UUID.randomUUID())
                        .build();
            }
            return mUserSensor;
        }

        public UserSensorReading read() throws IOException {
            //Log.d(TAG, "Reading Magnetometer");
            return new UserSensorReading(hmc5883L.getMagnitudes());
        }
    }

    public double[] getMagnitudes() throws IOException {
        double[] values = new double[3];
        values[0] = ((double)8/(double)4096)*(double) hmc5883L.getMagnitudeX();
        values[1] = ((double)8/(double)4096)*(double) hmc5883L.getMagnitudeY();
        values[2] = ((double)8/(double)4096)*(double) hmc5883L.getMagnitudeZ();
        return values;

    }
}