package com.example.android.Sensors;

import android.content.Context;
import android.os.Looper;

/**
 * This class gives me methods to allow the easier setup and reading of data in the main activity
 * for any sensor I want to put onto the boat.
 */
interface Sensors {


    /* Set up receivers for sensor */
    public void setUp(Context context);


    /* obtain whatever data we need */
    public void getData(Looper looper);


    /* Destroy sensor */
    public void destroy();
}
