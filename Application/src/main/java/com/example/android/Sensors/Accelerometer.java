package com.example.android.Sensors;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.io.IOException;

import static com.example.android.BoardDefaults.I2C_BUS_ACCELEROMETER;

/**
 * This class IS A Sensor and inherits its methods.
 *
 * Additional methods are the getAccel() methods which are used in the main activity.
 */
public class Accelerometer implements Sensors {

    private String AccelX;
    private String AccelY;
    private String AccelZ;

    private MMA8452SensorDriver mAccelerometerDriver;

    @Override
    public void setUp(Context context) {
        try {
            mAccelerometerDriver = new MMA8452SensorDriver(I2C_BUS_ACCELEROMETER);
        } catch (IOException e) {

        }

    }

    @Override
    public void getData(Looper looper) {
        final Handler mainHandler = new Handler(looper);

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                double[] mMagnitudes = new double[3];
                double[] mAccelerations = new double[3];

                try {
                    mAccelerations = mAccelerometerDriver.getMagnitudes();
                    AccelX = String.format("%.3f",mAccelerations[0]);
                    AccelY = String.format("%.3f",mAccelerations[1]);
                    AccelZ = String.format("%.3f",mAccelerations[2]);

                    mainHandler.postDelayed(this, 10);
                } catch (IOException e) {
                    // This is your code
                }
            }
        };
        mainHandler.post(myRunnable);
    }

    @Override
    public void destroy() {
        if(mAccelerometerDriver != null){
            mAccelerometerDriver = null;
        }
    }

    public String getAccelX(){
        return AccelX;
    }

    public String getAccelY(){
        return AccelY;
    }

    public String getAccelZ(){
        return AccelZ;
    }
}
