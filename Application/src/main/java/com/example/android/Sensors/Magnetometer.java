package com.example.android.Sensors;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.example.android.Sensors.HMC5883LSensorDriver;
import com.example.android.bluetoothchat.R;

import java.io.IOException;

import static com.example.android.BoardDefaults.I2C_BUS_MAGNETOMETER;
import static com.example.android.bluetoothchat.MainActivity.TAG;

/**
 * This class IS A Sensor and inherits its methods.
 *
 * Additional methods local to this class are the getMag() methods for us in the main activity.
 * */
public class Magnetometer implements Sensors {

    protected HMC5883LSensorDriver mMagnetometerDriver;
    private String MagX;
    private String MagY;
    private String MagZ;
    private String mCompassPointingRotation;

    @Override
    public void setUp(Context context) {
        try {
            mMagnetometerDriver = new HMC5883LSensorDriver(I2C_BUS_MAGNETOMETER);
//            mMagnetometerDriver.registerMagnetometerSensor();
            Log.i(TAG, "Magnetometer driver registered");
        } catch (IOException e) {
            Log.e(TAG, "Error initializing Magnetometer driver: ", e);
        }
    }

    @Override
    public void getData(Looper looper) {

        final Handler mainHandler = new Handler(looper);

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                double[] mMagnitudes = new double[3];

                try {
                    mMagnitudes = mMagnetometerDriver.getMagnitudes();
                    MagX = String.format("%.3f",mMagnitudes[0]);
                    MagY = String.format("%.3f",mMagnitudes[1]);
                    MagZ = String.format("%.3f",mMagnitudes[2]);

                    mCompassPointingRotation = String.format("%.3f", (360*Math.atan2(mMagnitudes[1], mMagnitudes[0]))/(2*Math.PI));


                    mainHandler.postDelayed(this, 10);
                } catch (IOException e) {
                    // pass
                }
            }
        };
        mainHandler.post(myRunnable);

    }

    @Override
    public void destroy() {
        if (mMagnetometerDriver != null) {
            mMagnetometerDriver = null;
        }
    }

    public String getHeading(){ return this.mCompassPointingRotation; }

    public String getMagX() { return this.MagX; }

    public String getMagY() { return this.MagY; }

    public String getMagZ() { return this.MagZ; }
}
