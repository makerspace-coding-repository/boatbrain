/*
 * Copyright 2019 JFrey
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.Sensors;

import android.hardware.Sensor;

import com.google.android.things.userdriver.UserDriverManager;
import com.google.android.things.userdriver.sensor.UserSensor;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by cagdas on 24.12.2016.
 * Update by JFrey on 3/28/19
 */

public class MMA8452SensorDriver implements AutoCloseable {

    private static final String TAG = MMA8452SensorDriver.class.getSimpleName();

    private static final String DRIVER_VENDOR = "";
    private static final String DRIVER_NAME = "MMA8452";
    private static final int DRIVER_VERSION = 1;
    private static final int DRIVER_RESOLUTION = 8; //±8 xGravity
    private static final float DRIVER_POWER = 3.6f; // Volt
    private static final int DRIVER_MAX_DELAY_US = Math.round(1000000.f / 0.75f);
    private static final int DRIVER_MIN_DELAY_US = Math.round(1000000.f / 75f);

    private MMA8452 mma8452;
    private AccelerometerUserDriver mUserDriver;

    public MMA8452SensorDriver(String bus) throws IOException {
        mma8452 = new MMA8452(bus);
    }

    public void registerAccelerometerSensor() {
        if (mma8452 == null) {
            throw new IllegalStateException("cannot register closed driver");
        }

        if (mUserDriver == null) {
            mUserDriver = new AccelerometerUserDriver();
            UserDriverManager.getInstance().registerSensor(mUserDriver.getUserSensor());
        }
    }

    public void unregisterAccelerometerSensor() {
        if (mUserDriver != null) {
            UserDriverManager.getInstance().unregisterSensor(mUserDriver.getUserSensor());
            mUserDriver = null;
        }
    }

    @Override
    public void close() throws Exception {
        unregisterAccelerometerSensor();
        if (mma8452 != null) {
            try {
                mma8452.close();
            } finally {
                mma8452 = null;
            }
        }
    }

    private class AccelerometerUserDriver {

        private UserSensor mUserSensor;

        private UserSensor getUserSensor() {
            if (mUserSensor == null) {
                mUserSensor = new UserSensor.Builder()
                        .setType(Sensor.TYPE_ACCELEROMETER)
                        .setName(DRIVER_NAME)
                        .setVendor(DRIVER_VENDOR)
                        .setVersion(DRIVER_VERSION)
                        .setResolution(DRIVER_RESOLUTION)
                        .setPower(DRIVER_POWER)
                        .setMinDelay(DRIVER_MIN_DELAY_US)
                        .setMaxDelay(DRIVER_MAX_DELAY_US)
                        .setUuid(UUID.randomUUID())
                        .build();
            }
            return mUserSensor;
        }

    }

    public double[] getMagnitudes() throws IOException {
        double[] values = new double[3];
        values[0] = ((double)1/(double)4096)*(double) mma8452.getMagnitudeX();
        values[1] = ((double)1/(double)4096)*(double) mma8452.getMagnitudeY();
        values[2] = ((double)1/(double)4096)*(double) mma8452.getMagnitudeZ();
        return values;

    }
}