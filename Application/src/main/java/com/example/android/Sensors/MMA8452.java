package com.example.android.Sensors;

import android.util.Log;

import com.google.android.things.pio.I2cDevice;
import com.google.android.things.pio.PeripheralManager;

import java.io.IOException;

public class MMA8452 implements AutoCloseable {
    private static final String TAG = MMA8452.class.getSimpleName();

    public static final int MMA8452_DEV_ADDR = 0x1D;
    private static final int STATUS_REG = 0x00,
            OUT_X_MSB = 0x01, OUT_X_LSB = 0x02,
            OUT_Y_MSB = 0x03, OUT_Y_LSB = 0x04,
            OUT_Z_MSB = 0x05, OUT_Z_LSB = 0x06,
            SYSMOD = 0x0B,
            INT_SOURCE = 0x0C,
            WHO_AM_I = 0x0D,
            XYZ_DATA_CFG_REG = 0x0E, XYZ_SENSITIVITY_8G = 0x02,
            HP_FILTER_CUTOFF = 0x0F,
            PL_STATUS = 0x10, PL_CFG = 0x11, PL_COUNT = 0x12, PL_BF_ZCOMP = 0x13,
            P_L_THS_REG = 0x14,
            FF_MT_CFG = 0x15, FF_MT_SRC = 0x16, FF_MT_THS = 0x17, FF_MT_COUNT = 0x18,
            TRANSIENT_CFG = 0x1D, TRANSIENT_SRC = 0x1E, TRANSIENT_THS = 0x1F, TRANSIENT_COUNT = 0x20,
            PULSE_CFG = 0x21, PULSE_SRC = 0x22, PULSE_THSX = 0x23, PULSE_THSY = 0x24, PULSE_THSZ = 0x25, PULSE_TMLT = 0x26, PULSE_LTCY = 0x27, PULSE_WIND = 0x28,
            ASLP_COUNT = 0x29,
            CTRL_REG1 = 0x2A, CTRL_REG2 = 0x2B, CTRL_REG3 = 0x2C, CTRL_REG4 = 0x2D, CTRL_REG5 = 0x2E,
            OFF_X = 0x2F, OFF_Y = 0x30, OFF_Z = 0x31;

    private I2cDevice mDevice;

    public MMA8452(String bus) throws IOException {
        PeripheralManager manager = PeripheralManager.getInstance();
        I2cDevice device = manager.openI2cDevice(bus, MMA8452_DEV_ADDR);

        try {
            connect(device);
            Log.d("MMA8452:", "Connect Complete");

        } catch (IOException | RuntimeException e) {
            try {
                close();
            } catch (IOException | RuntimeException ignored) {
            }
            throw e;
        }
    }

    @Override
    public void close() throws IOException {
        if (this.mDevice != null) {
            this.mDevice.close();
            this.mDevice = null;
        }
    }

    private void connect(I2cDevice device) throws IOException {
        if (mDevice != null) {
            throw new IllegalStateException("device already connected");
        }
        this.mDevice = device;

        //Insert Setup Code Here
        mDevice.writeRegByte(XYZ_DATA_CFG_REG, (byte) XYZ_SENSITIVITY_8G);
        mDevice.writeRegByte(CTRL_REG1,(byte) 0x19);
        //int val = mDevice.readRegByte(STATUS_REG);
        //Log.d(TAG, "MMA8452 Read: "+val);

    }

    public int getMagnitudeX() throws IOException {
        int lsb = this.mDevice.readRegByte(OUT_X_LSB) & 0xff;
        int msb = this.mDevice.readRegByte(OUT_X_MSB);
        //Log.d(TAG, "getMagX: " + (msb << 8 | lsb));
        return (msb << 8 | lsb);
    }

    public int getMagnitudeY() throws IOException {
        int lsb = this.mDevice.readRegByte(OUT_Y_LSB) & 0xff;
        int msb = this.mDevice.readRegByte(OUT_Y_MSB);
        //Log.d(TAG, "getMagY: " + (msb << 8 | lsb));
        return (msb << 8 | lsb);
    }

    public int getMagnitudeZ() throws IOException {
        int lsb = this.mDevice.readRegByte(OUT_Z_LSB) & 0xff;
        int msb = this.mDevice.readRegByte(OUT_Z_MSB);
        //Log.d(TAG, "getMagZ: " + (msb << 8 | lsb));
        return (msb << 8 | lsb);
    }


}
