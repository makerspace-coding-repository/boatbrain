package com.example.android.Sensors;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.GnssStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.OnNmeaMessageListener;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.google.android.things.contrib.driver.gps.NmeaGpsDriver;


import java.io.IOException;

import static android.app.Service.START_NOT_STICKY;
import static android.content.Context.LOCATION_SERVICE;
import static com.example.android.BoardDefaults.ACCURACY_GPS;
import static com.example.android.BoardDefaults.GPS_ACCURACY;
import static com.example.android.BoardDefaults.UART_BAUD_GPS;
import static com.example.android.BoardDefaults.UART_BUS_GPS;

/**
 *This class IS A Sensor and inherits its methods.
 *
 * Additional methods are the getLat()/Long() methods used in the main activity.
 *
 * Also ni this class are a few handlers and listeners integral to the GPS' functioning.
 */

public class GPS implements Sensors {
    private static final String TAG = GPS.class.getSimpleName();

    private Context context;
    private NmeaGpsDriver mGpsDriver;
    private LocationManager mLocationManager;
    private double latitude;
    private double longitude;

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            //Log.v(TAG, "Location update: " + location);
            //Log.d(TAG, "Location " + location.getLatitude() + ", " + location.getLongitude() + " was read");
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

    @Override
    public void setUp(Context context) {
        try {
            this.context = context;
            // Register the GPS driver
            mGpsDriver = new NmeaGpsDriver(context, UART_BUS_GPS, UART_BAUD_GPS, GPS_ACCURACY);
            mGpsDriver.register();
        } catch (IOException e) {
            Log.w(TAG, "Unable to open GPS UART", e);
        }



        mLocationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);

        // We need permission to get location updates
        if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // A problem occurred auto-granting the permission
            Log.d(TAG, "No permission");
            return;
        }
    }

    @Override
    public void getData(Looper looper) {
            // Register for location updates
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    0, 0, mLocationListener);
            mLocationManager.registerGnssStatusCallback(mStatusCallback);
            mLocationManager.addNmeaListener(mMessageListener);
    }

    @Override
    public void destroy(){

    }

    /** Report satellite status */
    private GnssStatus.Callback mStatusCallback = new GnssStatus.Callback() {
        @Override
        public void onStarted() { }

        @Override
        public void onStopped() { }

        @Override
        public void onFirstFix(int ttffMillis) { }

        @Override
        public void onSatelliteStatusChanged(GnssStatus status) {
            //Log.v(TAG, "GNSS Status: " + status.getSatelliteCount() + " satellites.");
            int satelliteCount = status.getSatelliteCount();
        }
    };

    /** Report raw NMEA messages **/
    private OnNmeaMessageListener mMessageListener = new OnNmeaMessageListener() {
        @Override
        public void onNmeaMessage(String message, long timestamp) {
            //Log.v(TAG, "NMEA: " + message);
            //mTextViewMid.setText("NMEA: " + message);

        }
    };

    public double getLongitude(){
        return this.longitude;
    }

    public double getLatitude(){
        return this.latitude;
    }
}
