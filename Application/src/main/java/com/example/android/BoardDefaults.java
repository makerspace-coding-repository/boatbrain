package com.example.android;

public class BoardDefaults {
    // UART - GPS Related
    public static final int UART_BAUD_GPS = 9600;
    public static final float GPS_ACCURACY = 2.5f;
    public static final String UART_BUS_GPS = "UART6";
    public static final float ACCURACY_GPS = 2.5f; // From GPS datasheet


    // I2C - HMC5883L 3 Axis Magnetometer
    public static final String I2C_BUS_MAGNETOMETER = "I2C1", I2C_BUS_ACCELEROMETER = "I2C1";

    // PWM - Motor Control Output
    // PWM1 - Motor Thrust
    public static final String PWM_BUS_VS = "PWM1";
    // PWM2 - Turning Servo X
    public static final String PWM_BUS_X = "PWM2";
    // PWM3 - Turning Servo Y
    public static final String PWM_BUS_Y = "PWM3";

    
    
}
