/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.bluetoothchat;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.Sensors.AdafruitServo;
import com.example.android.Sensors.AdafruitServoHat;
import com.example.android.Sensors.MiniPID;
import com.example.android.Sensors.PID;
import com.google.android.things.contrib.driver.ht16k33.AlphanumericDisplay;
import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.PeripheralManager;

import com.example.android.common.logger.Log;
import com.google.android.things.pio.Pwm;

import java.io.IOException;
import java.util.List;

import static android.os.Looper.getMainLooper;
import static com.example.android.bluetoothchat.MainActivity.heading;
import static com.example.android.bluetoothchat.MainActivity.isNumeric;
import static com.example.android.bluetoothchat.MainActivity.latitude;
import static com.example.android.bluetoothchat.MainActivity.longitude;
import static java.lang.Math.abs;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

/**
 * This fragment controls Bluetooth to communicate with other devices.
 * The important method here is handleMessage() which lets use strings as commands during runtime.
 * We also handle VSD and Servo motor control here, both manually using string commands and automatically
 * by clicking a map point on the phone client, which sends a string command. This can also be done manually
 * but probably isn't worth it.
 */
public class BluetoothChatFragment extends Fragment {

    private static final String TAG = "BluetoothChatFragment";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;
    private Pwm pwm;

    // Layout Views
    private ListView mConversationView;
    private EditText mOutEditText;
    private Button mSendButton;

    public static final String I2C_DEVICE_NAME = "I2C1";
    private static final int SERVO_HAT_I2C_ADDRESS = 0x40;

    private AdafruitServoHat mServoHat;

    double minAngle = -180.0;
    double maxAngle = 180.0;
    double minPulseDurationRange = 0.7; // ms
    double maxPulseDurationRange = 1.50; // ms
    int frequency = 40;
    double goToLat;
    double goToLong;
    private MiniPID ThrustPIDLoop = new MiniPID(10,5,0);
    private MiniPID HeadingPIDLoop = new MiniPID(1,0,0);
    double firstLatitude;
    double firstLongitude;
    double directionVectorX;
    double directionVectorY;
    double thrustVectorX;
    double thrustVectorY;
    double distanceFromWaypoint;

    public static double global_GoToHeading;
    public static double global_Theta;

    /**
     * Name of the connected device
     */
    private String mConnectedDeviceName = null;

    /**
     * Array adapter for the conversation thread
     */
    private ArrayAdapter<String> mConversationArrayAdapter;

    /**
     * String buffer for outgoing messages
     */
    private StringBuffer mOutStringBuffer;

    /**
     * Local Bluetooth adapter
     */
    private BluetoothAdapter mBluetoothAdapter = null;

    /**
     * Member object for the chat services
     */
    private BluetoothChatService mChatService = null;

    AlphanumericDisplay mDisplay;
    Gpio buttonGpio;
    Gpio buttonGpio1;
    Gpio RedGpio;
    Gpio GreenGpio;
    AdafruitServo servoX;
    AdafruitServo servoY;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            FragmentActivity activity = getActivity();
            Toast.makeText(activity, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            activity.finish();
        }

    }


    @Override
    public void onStart() {
        super.onStart();
        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else if (mChatService == null) {
            setupChat();
        }
        setupGPIO();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mChatService != null) {
            mChatService.stop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mChatService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mChatService.getState() == BluetoothChatService.STATE_NONE) {
                // Start the Bluetooth chat services
                mChatService.start();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bluetooth_chat, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mConversationView = (ListView) view.findViewById(R.id.in);
        mOutEditText = (EditText) view.findViewById(R.id.edit_text_out);
        mSendButton = (Button) view.findViewById(R.id.button_send);
    }

    /**
     * Set up the UI and background operations for chat.
     */
    private void setupChat() {
        Log.d(TAG, "setupChat()");

        // Initialize the array adapter for the conversation thread
        mConversationArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.message);

        mConversationView.setAdapter(mConversationArrayAdapter);

        // Initialize the compose field with a listener for the return key
        mOutEditText.setOnEditorActionListener(mWriteListener);

        // Initialize the send button with a listener that for click events
        mSendButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Send a message using content of the edit text widget
                View view = getView();
                if (null != view) {
                    TextView textView = (TextView) view.findViewById(R.id.edit_text_out);
                    String message = textView.getText().toString();
                    sendMessage(message);
                }
            }
        });

        // Initialize the BluetoothChatService to perform bluetooth connections
        mChatService = new BluetoothChatService(getActivity(), mHandler);

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = new StringBuffer("");
    }

    /**
     * Makes this device discoverable for 300 seconds (5 minutes).
     */
    private void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    /**
     * Sends a message.
     *
     * @param message A string of text to send.
     */
    protected void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (mChatService.getState() != BluetoothChatService.STATE_CONNECTED) {
            Toast.makeText(getActivity(), R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            mChatService.write(send);

            // Reset out string buffer to zero and clear the edit text field
            mOutStringBuffer.setLength(0);
            mOutEditText.setText(mOutStringBuffer);
        }
    }

    /**
     * The action listener for the EditText widget, to listen for the return key
     */
    private TextView.OnEditorActionListener mWriteListener
            = new TextView.OnEditorActionListener() {
        public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
            // If the action is a key-up event on the return key, send the message
            if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_UP) {
                String message = view.getText().toString();
                sendMessage(message);
            }
            return true;
        }
    };

    /**
     * Updates the status on the action bar.
     *
     * @param resId a string resource ID
     */
    private void setStatus(int resId) {
        FragmentActivity activity = getActivity();
        if (null == activity) {
            return;
        }
        final ActionBar actionBar = activity.getActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(resId);
    }

    /**
     * Updates the status on the action bar.
     *
     * @param subTitle status
     */
    private void setStatus(CharSequence subTitle) {
        FragmentActivity activity = getActivity();
        if (null == activity) {
            return;
        }
        final ActionBar actionBar = activity.getActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(subTitle);
    }

    /**
     * The Handler that gets information back from the BluetoothChatService. THIS IS THE MAGIC CODE FOR THIS WHOLE PROGRAM.
     *
     * Any commands sent from the phone to the board are handled here.
     * Commands:
     * MAP - Manual. Begins the Map activity
     * X - Manual. Move X servo to a location. Form: "X12"
     * Y - Manual. Same as X, different servo.
     * 4:15 - Any number between 4 and fifteen sent alone is used to control the VSD. Always send "4" on startup to calibrate the motor.
     *
     * GoTo Coordinates: lat/lng:    Automatic. Begins process for PID thrust control to guide towards a waypoint.
     */
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            FragmentActivity activity = getActivity();
            switch (msg.what) {
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothChatService.STATE_CONNECTED:
                            setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));
                            mConversationArrayAdapter.clear();
                            break;
                        case BluetoothChatService.STATE_CONNECTING:
                            setStatus(R.string.title_connecting);
                            break;
                        case BluetoothChatService.STATE_LISTEN:
                        case BluetoothChatService.STATE_NONE:
                            setStatus(R.string.title_not_connected);
                            break;
                    }
                    break;
                case Constants.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    mConversationArrayAdapter.add("Me:  " + writeMessage);
                    break;
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    mConversationArrayAdapter.add(mConnectedDeviceName + ":  " + readMessage);

                    // get the type of device that sent the message. If it was sent by phone to things, enable a light.
                    String model = Build.MODEL;
                    String model_1 = "iot_imx7d_pico";

                    //Send us to the maps Activity
                    if(model.equals(model_1) && readMessage.equals("MAP")){
                        //startActivity(new Intent(getContext(), MapsActivity.class));
                    }
                    //CONTROL MOTOR SPEED. *********MUST START BY SENDING "4" TO CALIBRATE MOTOR*********
                    else if(model.equals(model_1) && isNumeric(readMessage)){     //This should be between 4 and 15, where four is LowLow for calibration.
                        double d = Double.parseDouble(readMessage);
                        setMotorSpeed(d);
                    }
                    //MOVE SERVO X
                    else if(model.equals(model_1) && readMessage.substring(0,1).contains("X")){
                        String positional = readMessage.substring(1);
                        int d = Integer.parseInt(positional);
                        mServoHat.rotateToAngle(servoX, d);
                        Log.d(TAG, "Moved Servo X " + d);
                    }
                    //MOVE SERVO Y
                    else if(model.equals(model_1) && readMessage.substring(0,1).contains("Y")){
                        String positional = readMessage.substring(1);
                        int d = Integer.parseInt(positional);
                        mServoHat.rotateToAngle(servoY, d);
                        Log.d(TAG, "Moved Servo Y " + d);
                    }
                    //AUTOMATIC SERVO MOVEMENT BASED ON RELATIVE POSITION TO WAYPOINT
                    else if(model.equals(model_1) && readMessage.contains("GoTo Coordinates: lat/lng:")){

                        String parseableString = "";
                        int endLat = 0;

                        Log.d(TAG, "Coordinates received. Calculate thrust.");
                        Log.d(TAG, readMessage);

                        for(int i = 28; !readMessage.substring(28, i).contains(","); i++){
                            parseableString = readMessage.substring(28, i);
                            endLat = i;
                        }
                        goToLat = Double.parseDouble(parseableString);

                        parseableString = readMessage.substring(endLat + 1, readMessage.length() - 1);
                        goToLong = Double.parseDouble(parseableString);

                        //Repeat these every second.
                        final Handler mainHandler = new Handler(getMainLooper());
                        Runnable myRunnable = new Runnable() {
                            @Override
                            public void run() {
                                setThrustDirectionDirect(goToLat, goToLong);
                                setThrustMagnitude(goToLat, goToLong);
                                mainHandler.postDelayed(this, 1000);
                            }
                        };
                        mainHandler.post(myRunnable);


                        firstLatitude = latitude;
                        firstLongitude = longitude;

                    }

                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    if (null != activity) {
                        Toast.makeText(activity, "Connected to "
                                + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constants.MESSAGE_TOAST:
                    if (null != activity) {
                        Toast.makeText(activity, msg.getData().getString(Constants.TOAST),
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    };

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupChat();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(getActivity(), R.string.bt_not_enabled_leaving,
                            Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
        }
    }

    /**
     * Establish connection with other device
     *
     * @param data   An {@link Intent} with {@link DeviceListActivity#EXTRA_DEVICE_ADDRESS} extra.
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras()
                .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        mChatService.connect(device, secure);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.bluetooth_chat, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.secure_connect_scan: {
                // Launch the DeviceListActivity to see devices and do scan
                Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                return true;
            }
            case R.id.insecure_connect_scan: {
                // Launch the DeviceListActivity to see devices and do scan
                Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
                return true;
            }
            case R.id.discoverable: {
                // Ensure this device is discoverable by others
                ensureDiscoverable();
                return true;
            }
        }
        return false;
    }





    /**
    SETUP FOR BOATBRAIN SENSORS AND IO.
    */
    private void setupGPIO(){

        try {
            PeripheralManager manager = PeripheralManager.getInstance();

            Log.d(TAG, "SETTING UP");

            pwm = manager.openPwm("PWM1");

            mServoHat = new AdafruitServoHat(I2C_DEVICE_NAME, SERVO_HAT_I2C_ADDRESS);

            servoX = new AdafruitServo(0, frequency);
            servoX.setPulseDurationRange(minPulseDurationRange, maxPulseDurationRange);
            servoX.setAngleRange(minAngle, maxAngle);
            servoX.setAngle(90);
            mServoHat.addServo(servoX);

            servoY = new AdafruitServo(3, frequency);
            servoY.setPulseDurationRange(minPulseDurationRange, maxPulseDurationRange);
            servoY.setAngleRange(minAngle, maxAngle);
            servoY.setAngle(90);
            mServoHat.addServo(servoY);

//            sendMessage("4"); //Calibrates VSD so that I don't have to worry about sending it manually.



            Log.d(TAG, "ALL SET UP");



        }catch(IOException e){
            Log.d(TAG, "What?");
        }
    }


    /**
     * Set direction for Thrust Vector. Uses the steering method instead of a straight thrust.
     *
     * For the case of one motor only, I may be able to relate the heading to the waypoint angle instead.
     *
     * CURRENTLY DEPRECATED AND UNUSED.
     */
    public void setThrustDirectionSteering(double goToCoordsX, double goToCoordsY){
        //I know I'm going to need this.
        Log.d(TAG, Double.toString(heading));

        //radians
        double goToHeading = atan2(goToCoordsX - longitude, goToCoordsY - latitude);

        goToHeading *= (180 / 3.14159);

        //degrees
        goToHeading = (450 - (int)goToHeading) % 360;

        HeadingPIDLoop.setOutputLimits(-50, 50);

        double headingOutput = HeadingPIDLoop.getOutput(heading,goToHeading);


        heading *= (180 / 3.14159);

        heading = (450 - (int)heading) % 360;

        HeadingPIDLoop.setOutputLimits(-50, 50);


        if(heading < goToHeading + 10 && heading > goToHeading - 10){
            mServoHat.rotateToAngle(servoX, 0);
            Log.d(TAG, "Heading withing normal operating range; not changing course.");
        }
        if(goToHeading < heading){
            mServoHat.rotateToAngle(servoX, 50);
            Log.d(TAG, "X Servo going to 50" + heading);
        }
        else{
            mServoHat.rotateToAngle(servoX, -50);
            Log.d(TAG, "X Servo going to -50" + heading + ", " + goToHeading);
        }
        mServoHat.rotateToAngle(servoY, 50);
    }

    /**
     * Steering using the PID method to go straight towards a point instead of steering with tolerance.
     * @param goToCoordsX
     * @param goToCoordsY
     */
    public void setThrustDirectionDirect(double goToCoordsX, double goToCoordsY){
        //radians
        double goToHeading = 180 * (atan2(goToCoordsY - longitude, goToCoordsX - latitude)) / Math.PI;
        global_GoToHeading = goToHeading;

        double theta = goToHeading - heading;
        global_Theta = theta;

        theta = theta * (Math.PI / 180);

        thrustVectorX = 30 * (sin(theta));
        thrustVectorY = 30 * (cos(theta));

        Log.d(TAG, Double.toString(thrustVectorX));
        Log.d(TAG, Double.toString(thrustVectorY));

        mServoHat.rotateToAngle(servoX, (int)thrustVectorX);
        mServoHat.rotateToAngle(servoY, (int)thrustVectorY);
    }

    /**
     * Set magnitude for Thrust Vector. Used in Automatic boat control.
     */
    public void setThrustMagnitude(double goToCoordsX, double goToCoordsY){

        //Get the distance from the waypoint currently and
        //plug that into the PID loop to make the motor turn.
        directionVectorX = goToCoordsX - latitude;
        directionVectorY = goToCoordsY - longitude;

        distanceFromWaypoint = sqrt(pow(directionVectorX, 2) + pow(directionVectorY, 2));

        double output= abs(ThrustPIDLoop.getOutput(distanceFromWaypoint,0) * pow(10, 4));
//        Log.d(TAG, Double.toString(distanceFromWaypoint));

//        Log.d(TAG, Double.toString(output));
        setMotorSpeed(output);
    }


    /**
     * This method is used for manual and automatic speed control.
     *
     * In manual, the speed parameter is taken from the text input given by a user,
     * but in automatic setThrustMagnitude() gives the parameter.
     * @param speed
     */
    protected void setMotorSpeed(double speed){
        try {

            speed += 4;
            if(speed > 10){
                speed = 10;
            }

            Log.d(TAG, Double.toString(speed));

            pwm.setEnabled(false);

            pwm.setPwmFrequencyHz(50);
            pwm.setPwmDutyCycle(speed);

            pwm.setEnabled(true);
        }catch(Exception e){
            Log.d(TAG, "Problem Setting Motor Speed");
        }
    }
}
