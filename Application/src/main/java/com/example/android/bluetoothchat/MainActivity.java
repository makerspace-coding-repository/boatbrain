/*
* Copyright 2013 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


package com.example.android.bluetoothchat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.example.android.Sensors.Accelerometer;
import com.example.android.Sensors.GPS;
import com.example.android.Sensors.Magnetometer;
import com.example.android.common.activities.SampleActivityBase;
import com.example.android.common.logger.Log;
import com.example.android.common.logger.LogFragment;
import com.example.android.common.logger.LogWrapper;
import com.example.android.common.logger.MessageOnlyLogFilter;

import com.google.android.things.contrib.driver.gps.NmeaGpsDriver;

import java.io.IOException;

import static com.example.android.bluetoothchat.BluetoothChatFragment.global_GoToHeading;
import static com.example.android.bluetoothchat.BluetoothChatFragment.global_Theta;

/**
 * This is the launching point for all other classes and actions.
 * We start Bluetooth and data collection as well as data sending here.
 */
public class MainActivity extends SampleActivityBase {

    public static final String TAG = "MainActivity";

    // Whether the Log Fragment is currently shown
    private boolean mLogShown;

    private NmeaGpsDriver mGpsDriver;
    private TextView mTextViewTop;
    private TextView mTextViewMid;
    private TextView mTextViewBot;
    private Magnetometer mag;
    private GPS gps;
    private Accelerometer acc;
    BluetoothChatFragment fragment;
    protected double thrustVector;
    protected double compassVector;
    protected static double latitude;
    protected static double longitude;
    protected static double heading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mTextViewTop = (TextView) findViewById(R.id.text_view_top);
        mTextViewMid = (TextView) findViewById(R.id.text_view_mid);
        mTextViewBot = (TextView) findViewById(R.id.text_view_bot);

        if (savedInstanceState == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            fragment = new BluetoothChatFragment();
            transaction.replace(R.id.sample_content_fragment, fragment);
            transaction.commit();
        }

        //Data Acquisition Thread
        getSensorData();

        //Communications Thread
        sendSensorData();
    }



    @Override
    protected void onDestroy(){
        super.onDestroy();
        mag.destroy();
        gps.destroy();
        acc.destroy();
    }

    /**
     * Set up all sensors and begin collecting data from them (in a separate thread) 10x/sec
     * */
    protected void getSensorData(){
        //Set up and read from Magnetometer
        mag = new Magnetometer();
        mag.setUp(this);
        mag.getData(this.getMainLooper());

        //Set up and read from GPS
        gps = new GPS();
        gps.setUp(this);
        gps.getData(this.getMainLooper());

        //Set up and read from Accelerometer
        acc = new Accelerometer();
        acc.setUp(this);
        acc.getData(this.getMainLooper());

        final Handler mainHandler = new Handler(this.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                mTextViewTop.setText("Mag X: " + mag.getMagX() + "\nMag Y: " + mag.getMagY() + "\nMag Z: " + mag.getMagZ());
                mTextViewMid.setText("Location: \n" + String.format("%.3f", gps.getLatitude()) + " Latitude \n" + String.format("%.3f",gps.getLongitude()) + " Longitude");
                mTextViewBot.setText("Accelerations: \n Acc X: " + acc.getAccelX()+ " G\n Acc Y: " + acc.getAccelY() + " G \n Acc Z: " + acc.getAccelZ() + " G");

                //give static variables the lat and long for use in thrust calculation
                //Also give us the magnetometer heading to help with that as well.
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
                heading = Double.parseDouble(mag.getHeading());


                //I don't need a keyboard and this worked to make it go away.
                //I'm sure there's a better way to do it because now it won't come back.
                View view = getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }





                mainHandler.postDelayed(this, 10);
            }
        };
        mainHandler.post(myRunnable);
    }

    /**
     * Take the data from getSensorData() and send it to the phone once per second.
     *
     * TODO: make this a running average.
     * */
    private void sendSensorData(){
        final Handler mainHandler = new Handler(this.getMainLooper());

        Runnable dataSender = new Runnable() {
            @Override
            public void run() {

                String sendString = "Mag X: " + mag.getMagX() + " Mag Y: " + mag.getMagY() + " Mag Z: " + mag.getMagZ() + "\n"
                        + "Acc X: " + acc.getAccelX() + " Acc Y " + acc.getAccelY() + " Acc Z: " + acc.getAccelZ();

                String locString = "Lat: " + gps.getLatitude() + " Long: " + gps.getLongitude();

                fragment.sendMessage(sendString);
                fragment.sendMessage(locString);
                fragment.sendMessage("Heading " + mag.getHeading());
                fragment.sendMessage("GoToHeading " + Double.toString(global_GoToHeading));
                fragment.sendMessage("Theta " + Double.toString(global_Theta));


                mainHandler.postDelayed(this, 1000);
            }
        };
        mainHandler.post(dataSender);
    }




















    //All Methods below are inherited from the base blutooth project.
    //I am afraid to take them out but have no use for them.

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem logToggle = menu.findItem(R.id.menu_toggle_log);
        logToggle.setVisible(findViewById(R.id.sample_output) instanceof ViewAnimator);
        logToggle.setTitle(mLogShown ? R.string.sample_hide_log : R.string.sample_show_log);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_toggle_log:
                mLogShown = !mLogShown;
                ViewAnimator output = (ViewAnimator) findViewById(R.id.sample_output);
                if (mLogShown) {
                    output.setDisplayedChild(1);
                } else {
                    output.setDisplayedChild(0);
                }
                supportInvalidateOptionsMenu();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /** Create a chain of targets that will receive log data */
    @Override
    public void initializeLogging() {
        // Wraps Android's native log framework.
        LogWrapper logWrapper = new LogWrapper();
        // Using Log, front-end to the logging chain, emulates android.util.log method signatures.
        Log.setLogNode(logWrapper);

        // Filter strips out everything except the message text.
        MessageOnlyLogFilter msgFilter = new MessageOnlyLogFilter();
        logWrapper.setNext(msgFilter);

        // On screen logging via a fragment with a TextView.
        LogFragment logFragment = (LogFragment) getSupportFragmentManager()
                .findFragmentById(R.id.log_fragment);
        msgFilter.setNext(logFragment.getLogView());

        Log.i(TAG, "Ready");
    }

    public static boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
}
